const Lock = (props) => {
  return (
    <button onClick={props.lock}>
      {props.statelocked ? "Destravar" : "Travar"}
    </button>
  );
};

export default Lock;
