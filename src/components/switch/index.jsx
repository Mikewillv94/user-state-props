const Switch = (props) => {
  return (
    <>
      <button onClick={props.showhide} disabled={props.statelocked}>
        {props.state ? "Escoder" : "Mostar"}
      </button>
    </>
  );
};
export default Switch;
