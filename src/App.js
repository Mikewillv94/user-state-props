import logo from "./logo.svg";
import "./App.css";
import { useState } from "react";
import Switch from "./components/switch";
import Lock from "./components/Lock";

const App = () => {
  const [showlogo, setShowLogo] = useState(true);
  const [locked, setLocked] = useState(false);

  const ShowHide = () => {
    setShowLogo(!showlogo);
  };

  const Locked = () => {
    setLocked(!locked);
  };

  return (
    <div className="App">
      <header className="App-header">
        {showlogo && <img src={logo} className="App-logo" alt="logo" />}
        <Switch
          showhide={ShowHide}
          statelocked={locked}
          state={showlogo}
        ></Switch>
        <br></br>
        <Lock lock={Locked} statelocked={locked}></Lock>
      </header>
    </div>
  );
};

export default App;
